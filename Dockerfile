FROM node:alpine

WORKDIR /react

COPY package.json /react

RUN npm install --force

COPY . .

CMD ["npm", "start"]
