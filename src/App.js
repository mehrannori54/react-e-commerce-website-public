import React, {useEffect, useState} from 'react';
import './App.css';
import ProductSlider from './components/product_list/product_slider';
import PostLoadingComponent from './components/product_list/Loading';
import axiosInstance from './axios/axios';

function App() {

    const PostLoading = PostLoadingComponent(ProductSlider);
    const [appState, setAppState] = useState({
        loading: true,
        products: null,
    });

    useEffect(() => {
        axiosInstance.get('product/list').then((res) => {
            const allProducts = res.data;
            console.log(res.data);
            setAppState({loading: false, products: allProducts});
            console.log(res.data);
        });
    }, [setAppState]);
    return (
        <div className="App">
            <h1></h1>
            <PostLoading isLoading={appState.loading} products={appState.products}/>
        </div>
    );
}

export default App;

