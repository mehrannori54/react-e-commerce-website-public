import axios from 'axios';


// const baseURL = 'http://127.0.0.1:8000/api/';
// const baseURL = 'https://mehrannoori.ir/api/';
const DEBUG = process.env.REACT_APP_AXOIS_URl;
const isDEBUG = (DEBUG === 'true');

let baseURL = process.env.REACT_APP_AXOIS_URl + 'api/';

if (isDEBUG) {
    baseURL = 'http://127.0.0.1:8000/api/';
}


const axiosInstance = axios.create({
    baseURL: baseURL,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        accept: 'application/json',
    },
});

export default axiosInstance;