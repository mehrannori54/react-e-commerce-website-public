import axios from 'axios';


// const baseURL = 'http://127.0.0.1:8000/';
const DEBUG = process.env.REACT_APP_AXOIS_URl;
const isDEBUG = (DEBUG === 'true');

let baseURL = process.env.REACT_APP_AXOIS_URl;

if (isDEBUG) {
    baseURL = 'http://127.0.0.1:8000/';
}


const axiosInstance = axios.create({
    baseURL: baseURL,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        accept: 'application/json',
    },
});

export default axiosInstance;