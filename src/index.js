import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.sass';
import HomePage from './pages/home_page/HomePage';
import reportWebVitals from './reportWebVitals';
import {Route, BrowserRouter as Router, Routes} from 'react-router-dom';
import Header from './components/header/header';
import Footer from './components/footer';
import Login from './components/auth/login'
import ProductPage from "./pages/product_page/ProductPage";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Router>
        <React.StrictMode>
            <Header/>
            <Routes>
                <Route exact path="/" element={<HomePage/>}/>
                <Route exact path="/login" element={<Login/>}/>
                <Route exact path="/product/:slug" element={<ProductPage/>}/>
            </Routes>
            <Footer/>
        </React.StrictMode>
    </Router>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
