const ThreeDigitCurrency = (product_price) => {
    let rial;
    rial = new Intl.NumberFormat('fa', {style: 'decimal'}).format(product_price)
    return rial
}
export default ThreeDigitCurrency;