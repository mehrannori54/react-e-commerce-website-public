import {NavLink} from 'react-router-dom';
import SiteLogo from '../static/logo/logoipsum-280.svg';
import Banner from '../static/image/f469f1dbc30f134c681a554e332ac61487f831bd_1681244286.jpg'
import {useState, useEffect} from 'react';
import Cookies from 'universal-cookie';
import Navbar from "./Navbar";
import './css/header.sass';

const cookies = new Cookies();

function NavBarLeft() {


    const [authenticated, setAuthenticated] = useState(false);
    // useEffect(() => {(() =>)
    //     (cookies.get('access_token').then((res) => {
    //         console.log(res)
    //     }))
    // });
    useEffect(() => {
        const loggedInUser = cookies.get('access_token');
        if (loggedInUser) {
            setAuthenticated(true);
        }
    }, []);
    // if (cookies.get('access_token')) {
    //
    //     console.log(true);
    //     setAuthenticated(true)
    //
    // } else {
    //     setAuthenticated(false)
    // }

    if (authenticated) {
        return AuthenticateNav
    }
    return UnAuthenticateNav


    // authenticated ? return authenticateNav : return unauthenticateNav;
}

const AuthenticateNav = (
    <div className='d-flex justify-content-end navbar-btn-left align-items-center'>
        <div className='nav-icon-container'>
            <i className="fa-solid fa-caret-up fa-rotate-180 fa-2xs"/>
            <span className="fa-light fa-user fa-lg nav-icon"/>
        </div>
        <div className="vr vr-blurry"/>
        <div className='nav-icon-container'>
            <span className="fa-light fa-cart-shopping fa-lg nav-icon "/>
        </div>
    </div>
)

const UnAuthenticateNav = (
    <div className='d-flex justify-content-end navbar-btn-left  '>
        <NavLink className='' to='/login'>
            <div className='nav-icon-container d-flex align-items-center justify-content-around '
            >
                <i
                    className="fa-regular fa-arrow-right-to-bracket fa-rotate-180 nav-icon"
                    style={{'paddingLeft': 0}}/> ورود
                |
                ثبت نام

            </div>
        </NavLink>
        {/*<div className="vr vr-blurry"/>*/}
        {/*<NavLink to='/signup'>*/}
        {/*    <div className='nav-icon-container'>*/}
        {/*        <i className="fa-light fa-user-plus fa-lg nav-icon "/>*/}
        {/*    </div>*/}
        {/*</NavLink>*/}
    </div>
)


function Header() {
    return (
        <nav className="sticky-top d-flex w-100 rtl flex-column ">
            <div className="d-flex w-100 justify-content-center align-items-center banner">
                <img src={Banner} alt='site-banner' height='60px' width='100%'/>
            </div>
            <div className="d-flex justify-content-start nav-2">

                <div className="d-flex logo ">
                    <NavLink to='/'>
                        <img className='' src={SiteLogo} alt="site-logo"/>
                    </NavLink>
                </div>

                <hr className='hr'/>

                <div className="d-flex flex-grow-1 nav-inner align-items-center">

                    <div className="d-flex search-bar justify-content-start align-items-center ">
                        <span className="fa fa-search "/>

                        <input className="d-flex align-self-lg-auto flex-fill " type="text" name="search-input"
                               placeholder="جستجو"
                               autoComplete="off"/>
                    </div>

                    {/*<div className='d-flex justify-content-end navbar-btn-left align-items-center'>*/}
                    {/*    <div className='nav-icon-container'>*/}
                    {/*        <i className="fa-solid fa-caret-up fa-rotate-180 fa-2xs"/>*/}
                    {/*        <span className="fa-light fa-user fa-lg nav-icon"/>*/}
                    {/*    </div>*/}
                    {/*    <div className="vr vr-blurry"/>*/}
                    {/*    <div className='nav-icon-container'>*/}
                    {/*        <span className="fa-light fa-cart-shopping fa-lg nav-icon "/>*/}
                    {/*    </div>*/}

                    {/*</div>*/}
                    <NavBarLeft/>
                </div>
            </div>

            <Navbar/>
        </nav>


    );
}

// const NavBarLeft =


export default Header;
