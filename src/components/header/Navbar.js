import React, {useEffect, useState} from 'react'
import './css/header.sass'
import useScrollListener from "./useScrollListener";


const Navbar = () => {
    const [navClassList, setNavClassList] = useState([]);
    const scroll = useScrollListener();

    // update classList of nav on scroll
    useEffect(() => {
        const _classList = [' nav-scroll d-flex ',];

        if (scroll.y - scroll.lastY > 0)
            _classList.push("nav-bar--hidden");

        setNavClassList(_classList);
    }, [scroll.y, scroll.lastY]);


    return (
        <div className={navClassList.join("")}>
            <div className=" d-flex justify-contents-start align-items-center">

                <div className=" d-flex nav-3-pill dropdown-pill ">
                    <i className="fa  fa-solid fa-navicon align-self-center "/>
                    <div className='align-self-center'>دسته‌بندی کالاها</div>
                </div>
                <div className="vr "/>
                <div className=" d-flex nav-3-pill  ">
                    <i className="fa fa-sharp-solid fa-fire-flame-curved align-self-center  "/>
                    <div>پرفروش‌ترین‌ها</div>
                </div>
                <div className=" d-flex nav-3-pill ">
                    <i className="fa fa-solid fa-percentage align-self-center  "/>
                    <div>تخفیف‌ها و پیشنهادها</div>
                </div>


            </div>

        </div>

    )
}

export default Navbar