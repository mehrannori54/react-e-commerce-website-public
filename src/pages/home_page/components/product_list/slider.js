import React, {useEffect} from "react";
import {gsap} from "gsap";
import {Draggable} from "gsap/Draggable";
import './css/product-card.sass'
import {NavLink} from "react-router-dom";
import TomanSvg from "../../../../components/static/svg/toman.svg"
import {ReactSVG} from "react-svg";

gsap.registerPlugin(Draggable);

let rial = 0

const ThreeDigitCurrncy = (product_price) => {
    rial = new Intl.NumberFormat('fa', {style: 'decimal'}).format(product_price)
    return rial

}


const Slide = ({product}) => {
    return (
        <div className=" d-flex slide " id="slide">
            <NavLink style={{display: 'contents'}} to={'product/' + product.slug}>
                <div className='d-flex align-c flex-wrap product-card  ' key={product.id}>


                    <div className="d-flex flex-wrap  align-self-start  justify-content-center">
                        <div className='d-flex product-img '>
                            <img className='' src={product.image} title={product.image_alt}
                                 alt={product.image_alt}/>
                        </div>

                        <div className='d-flex product-info-box align-items-start '>
                            <div className='product-info rtl '>
                                {product.title.substr(0, 50)}
                            </div>
                            <div className="d-flex"/>
                        </div>
                    </div>
                    <div className="d-flex align-items-end ">
                        <div className='d-flex product-price  '>
                            <div className='d-flex price-icon'>
                                <ReactSVG src={TomanSvg}/>
                            </div>
                            <div className='d-flex price rtl '>
                                {ThreeDigitCurrncy(product.price)}
                            </div>
                        </div>
                    </div>
                </div>


            </NavLink>
        </div>
    );
};


const ProductSlider = (props) => {

    // const sliderRef = useRef(null);
    const {products} = props;
    console.log(props)
    console.log(products)


    useEffect(() => {
        Draggable.create('.draggable', {
            bounds: '#slider-container',
            edgeResistance: 0.85,
            type: "x",
            throwProps: true,
            autoScroll: true,
        })
    }, []);

    if (!products || products.length === 0) return <p>Can not find any posts, sorry</p>;

    return (
        <div>
            <div
                className='d-flex slider-container '>
                <div
                    id="slider-container"
                    className="d-flex slider-warper flex-no-wrap flex-row-reverse">
                    <div id="slider-red" className="slider d-flex draggable    ">
                        {products.map((product) => {
                            return (
                                <Slide key={product.id} product={product}/>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductSlider;
