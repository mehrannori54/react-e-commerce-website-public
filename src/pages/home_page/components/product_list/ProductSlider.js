import React, {useEffect, useState} from 'react';
import ProductSlider from './slider';
import LoadingComponent from '../loading/Loading';
import axiosInstance from '../../../../axios/axios';

function ProductsSlider() {

    const PostLoading = LoadingComponent(ProductSlider);
    const [appState, setAppState] = useState({
        loading: true,
        products: null,
    });

    useEffect(() => {
        axiosInstance.get('product/list').then((res) => {
            const allProducts = res.data;
            console.log(res.data);
            setAppState({loading: false, products: allProducts});
            console.log(res.data);
        });
    }, [setAppState]);
    return (
        <div className="App">
            <PostLoading isLoading={appState.loading} products={appState.products}/>
        </div>
    );
}

export default ProductsSlider;