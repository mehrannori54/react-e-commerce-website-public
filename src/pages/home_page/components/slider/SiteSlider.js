import {NavLink} from "react-router-dom";
import React, {useRef, useState} from "react";
import {Swiper, SwiperSlide} from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import './css/site_slider.sass';
// import required modules
import {Navigation, Pagination, Mousewheel, Keyboard, EffectCreative, Autoplay} from "swiper";


const SiteSlider = (props) => {
    const {slides} = props;
    console.log(slides)


    if (!slides || slides.length === 0) return <p>Can not find any slide, sorry</p>;
    return (
        <>
            <Swiper
                style={{
                    "--swiper-navigation-color": "#fbfbfb",
                    "--swiper-navigation-size": "25px",
                    "--swiper-pagination-color": "#fbfbfb",
                }}
                slidesPerView={1}
                loop={true}
                navigation={true}
                autoplay={{
                    delay: 10000,
                    disableOnInteraction: false,
                }}
                pagination={{
                    clickable: true,
                }}
                mousewheel={true}
                keyboard={true}
                lazy={true}
                effect={"creative"}
                creativeEffect={{
                    prev: {
                        shadow: true,
                        translate: ["-20%", 0, -1],
                    },
                    next: {
                        translate: ["100%", 0, 0],
                    },
                }}
                modules={[Navigation, Pagination, Mousewheel, Keyboard, EffectCreative, Autoplay]}
                className="mySwiper"
            >
                {slides.map((slide) => {
                    return (
                        <SwiperSlide>
                            <NavLink to={slide.url}>
                                <img src={slide.image} alt={slide.title} title={slide.url_title} loading="lazy"/>
                                <div className="swiper-lazy-preloader swiper-lazy-preloader-white"/>
                            </NavLink>
                        </SwiperSlide>

                    )
                })}
            </Swiper>
        </>
    );

};

export default SiteSlider