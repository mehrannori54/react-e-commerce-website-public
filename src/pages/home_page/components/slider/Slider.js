import React, {useEffect, useState} from 'react';
import SiteSlider from './SiteSlider';
import LoadingComponent from '../loading/Loading';
import axiosInstance from '../../../../axios/axios';


function Slider() {

    const SliderLoading = LoadingComponent(SiteSlider);
    const [appState, setAppState] = useState({
        loading: true,
        slides: null,
    });

    useEffect(() => {
        axiosInstance.get('slider/').then((res) => {
            const allSlides = res.data;
            console.log(res.data);
            setAppState({loading: false, slides: allSlides});
            console.log(res.data);
        });
    }, [setAppState]);
    return (
        <>
            <SliderLoading isLoading={appState.loading} slides={appState.slides}/>
        </>
    );
}

export default Slider;