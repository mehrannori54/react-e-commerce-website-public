// import React, {useEffect, useState} from 'react';
import ProductsSlider from './components/product_list/ProductSlider';
import Slider from './components/slider/Slider';
// import LoadingComponent from './components/loading/Loading';
// import axiosInstance from '../../axios/axios';

function HomePage() {
    return (
        <>
            <Slider/>
            <ProductsSlider/>
        </>
    );


}

export default HomePage;