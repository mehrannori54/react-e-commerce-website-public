import React, {useEffect, useState} from 'react'
import LoadingComponent from "../Loading/ProductLoading";
import axiosInstance from "../../../../axios/axios";
import {useParams} from "react-router-dom";
import ProductDisplay from "./ProductDisplay"


function SingleProduct() {

    const {slug} = useParams();
    const ProductLoading = LoadingComponent(ProductDisplay);
    const [appState, setAppState] = useState({
        loading: true,
        product_data: null,
    });


    useEffect(() => {
            axiosInstance.get('product/' + slug).then((res) => {
                const data = res.data;

                data.product_images.unshift({image: data.image, alt_text: data.image_alt});
                console.log(data)
                setAppState({
                    loading: false,
                    product_data: data
                });
            });
        },
        [setAppState, slug]);
    return (
        <>
            <ProductLoading isLoading={appState.loading} product_data={appState.product_data}/>

        </>
    );
}

export default SingleProduct;

