import './css/main_sector.sass'
import {useState, createContext, useContext} from "react";
import MediaQuery, {useMediaQuery} from 'react-responsive';
import {ReactSVG} from "react-svg";
import TomanSvg from "../../../../../components/static/svg/toman.svg"
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

import SmPicture from "./components/SmPicture";
import LgPicture from "./components/LgPicture";
import MiddleDetails from "./components/MiddleDetails";
import SemiFooter from "./components/SemiFooter";

import ThreeDigitCurrency from "../../../../../components/utils/utils"
// import component from "react-responsive/src/Component";


export const SlideContext = createContext('1');
export const SlideProvider = (props) => {
    const [slideNo, setSlide] = useState('1');

    return (
        <SlideContext.Provider value={[slideNo, setSlide] = ""}>
            {props.children}
        </SlideContext.Provider>
    )
}

function MainSector(props) {
    const {product} = props;

    //if (!product_data || product_data.length === 0) return <p>Can not find any slide, sorry</p>;
    return (
        <>
            <MediaQuery maxWidth={992}>
                <div className="d-flex justify-content-between bottom-box flex-nowrap rtl">
                    <div className="d-flex ">
                        <button className='btn btn-add-to-cart '>
                            افزودن به سبد
                        </button>
                    </div>
                    <div className="d-flex flex-nowrap flex-row align-self-center ">
                        {ThreeDigitCurrency(product.price)}
                        <ReactSVG style={{padding: "0 5px"}} src={TomanSvg}/>

                    </div>
                </div>
            </MediaQuery>
            <div
                className="d-flex flex-lg-row flex-column-reverse w-100 custom-container justify-content-lg-end justify-content-center">

                <div className="d-flex flex-grow-1 product-box flex-column flex-fill justify-content-center">


                    <div className="d-flex flex-column justify-content-start flex-grow-1 flex-fill ">
                        <p className="product-title rtl">{product.title}</p>
                        <div
                            className='d-flex flex-nowrap  align-items-lg-start flex-column flex-lg-row'>

                            <div className='d-flex product-box-left justify-content-center align-items-start box-sm'>
                                <div className="d-flex flex-column flex-fill justify-content-end rtl">

                                    <div className="d-flex padding-5-10">
                                        گارانتی ۱۲ ماهه
                                    </div>

                                    <hr className="hr hr-custom flex-grow-1 "/>

                                    <div className="d-flex padding-5-10">
                                        موجود در انبار
                                    </div>

                                    <MediaQuery minWidth={992}>
                                        <hr className="hr hr-custom flex-grow-1 "/>
                                        <div className="d-flex padding-5-10 ">

                                            {ThreeDigitCurrency(product.price)}
                                        </div>
                                        <div className="d-flex flex-grow-1">
                                            <button className='btn btn-add-to-cart flex-grow-1'>
                                                افزودن به سبد
                                            </button>
                                        </div>
                                    </MediaQuery>


                                </div>
                            </div>
                            <div className="d-flex flex-column flex-grow-1 ">

                                <div className="d-flex flex-row  flex-grow-1  sub-title">
                                    <div className="d-flex flex-grow-1">
                                        <hr className="hr hr-custom flex-grow-1 "/>
                                    </div>
                                    <div className="product-en-title flex-grow-0">
                                        {product.brand.title}
                                    </div>

                                </div>
                                <div
                                    className="d-flex flex-column rtl box-sm justify-content-start middle-details-box">
                                    <h5>ویژگی ها</h5>
                                    <MiddleDetails details={product.product_specification_value}/>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <MediaQuery minWidth={992}>
                    <LgPicture main_images={product.image} alt={product.image_alt} gallery={product.product_images}/>
                </MediaQuery>
                <MediaQuery maxWidth={992}>
                    <SmPicture main_images={product.image} alt={product.image_alt} gallery={product.product_images}/>
                </MediaQuery>

            </div>
            <SemiFooter/>
        </>


    )

}

export default MainSector