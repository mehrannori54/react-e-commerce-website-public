import {ReactSVG} from "react-svg";
import cash_on_delivery_SVG from "../../../../../../components/static/svg/cash-on-delivery.svg"
import days_return_SVG from "../../../../../../components/static/svg/days-return.svg"
import express_delivery_SVG from "../../../../../../components/static/svg/express-delivery.svg"
import original_products_SVG from "../../../../../../components/static/svg/original-products.svg"
import support_SVG from "../../../../../../components/static/svg/support.svg"

function SemiFooter() {
    return (

        <div
            className="d-flex custom-container semi-footer flex-grow-1 justify-content-around align-content-center rtl color-400">

            <div className="d-flex flex-column flex-lg-row justify-content-center align-content-center">

                <ReactSVG className="d-flex box-svg justify-content-center" src={express_delivery_SVG}/>
                <div className="d-flex">
                    <p className="text-caption-strong">امکان تحویل اکسپرس</p>
                </div>
            </div>

            <div className="d-flex flex-column flex-lg-row justify-content-center align-content-center">

                <ReactSVG className="d-flex box-svg justify-content-center" src={support_SVG}/>
                <div className="d-flex">
                    <p className="text-caption-strong">
                        ۲۴ ساعته، ۷ روز هفته
                    </p>
                </div>
            </div>

            <div className="d-flex flex-column flex-lg-row justify-content-center align-content-center">

                <ReactSVG className="d-flex box-svg justify-content-center" src={cash_on_delivery_SVG}/>
                <div className="d-flex">
                    <p className="text-caption-strong">
                        امکان پرداخت در محل
                    </p>
                </div>
            </div>

            <div className="d-flex flex-column flex-lg-row justify-content-center align-content-center">

                <ReactSVG className="d-flex box-svg justify-content-center" src={days_return_SVG}/>
                <div className="d-flex">
                    <p className="text-caption-strong">
                        هفت روز ضمانت بازگشت کالا
                    </p>
                </div>
            </div>

            <div className="d-flex flex-column flex-lg-row justify-content-center align-content-center">

                <ReactSVG className="d-flex box-svg justify-content-center" src={original_products_SVG}/>
                <div className="d-flex">

                    <p className="text-caption-strong">
                        ضمانت اصل بودن کالا
                    </p>
                </div>
            </div>

        </div>
    )
}

export default SemiFooter;