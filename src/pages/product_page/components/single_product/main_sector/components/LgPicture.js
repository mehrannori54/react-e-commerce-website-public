import ReactImageMagnify from "react-image-magnify";
import {useEffect, useRef, useState} from "react";
import {useMediaQuery} from "react-responsive";
import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination, Mousewheel, Keyboard} from "swiper";


const LgPicture = ({main_images, gallery, alt}) => {

    return (
        <div className="d-flex flex-shrink-0 product_gallery">
            <div className="">
                <div className="">
                    <ReactImageMagnify {...{
                        smallImage: {
                            alt: alt,
                            isFluidWidth: true,
                            src: main_images,
                        },
                        largeImage: {
                            src: main_images,
                            width: 1400,
                            height: 1400
                        },
                        shouldUsePositiveSpaceLens: false,
                        enlargedImagePosition: "beside",
                        enlargedImageContainerClassName: 'rtl-large-image',
                        enlargedImageContainerDimensions: {
                            width: '170%',
                            height: '90%'
                        }
                    }} />
                </div>
                <ProductGallery gallery={gallery}/>

            </div>

        </div>
    )
}
const GalleryModel = ({gallery, slideNo}) => {
    const swiperRef = useRef(null);
    // const buttonRef = useRef(null);

    useEffect(() => {
        toSlide(slideNo)
    }, [slideNo]);

    const toSlide = (num) => {
        console.log(slideNo + "get")
        console.log("go to slide", num);
        swiperRef.current?.swiper.slideTo(num);
    };

    const handleClick = (e) => {
        e.preventDefault();
        toSlide(e.target.id);

    }
    const slide = [];
    const thumbs = [];
    gallery.map((image, i) => {

        slide.push(
            <SwiperSlide>
                <img src={image.image} alt={image.alt_text} className=""/>
            </SwiperSlide>
        )
        return (slide)
    })
    gallery.map((image, i) => {

        thumbs.push(
            <div className="d-flex gallery-content justify-content-center align-items-center model-gallery"
                 key={i}>
                <img src={gallery[i].image} alt={gallery[i].alt_text} className="d-flex" id={i} onClick={handleClick}/>
            </div>
        )
        return (slide)
    })
    return (
        <div className="modal modal-xl fade " id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"/>
                    </div>
                    <div className="modal-body">
                        <div className="d-flex">

                        </div>

                        <div className="d-flex">
                            <div className="d-flex flex-wrap justify-content-start align-content-start">
                                {thumbs}
                            </div>
                            <div className="d-flex product_gallery rtl w-75 h-75">
                                <Swiper
                                    ref={swiperRef}
                                    style={{
                                        "--swiper-navigation-color": "#fd2727",
                                        "--swiper-navigation-size": "25px",
                                        "--swiper-pagination-color": "#fbfbfb",
                                    }}
                                    slidesPerView={1}
                                    navigation={true}
                                    reverseDirection={true}
                                    pagination={{
                                        clickable: true,
                                    }}
                                    lazy={true}
                                    mousewheel={true}
                                    keyboard={true}
                                    modules={[Navigation, Pagination, Mousewheel, Keyboard]}
                                    className="mySwiper2"
                                >
                                    {slide}
                                </Swiper>


                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>)

}
const ProductGallery = ({gallery}) => {
    const [slideNum, setSlide] = useState('');
    const handleClick = (e) => {
        e.preventDefault();
        setSlide(e.target.id)
    }
    let imageCount;
    if (useMediaQuery({minWidth: 1515})) {
        imageCount = 6;
    }
    if (useMediaQuery({minWidth: 1270, maxWidth: 1515})) {
        imageCount = 5;
    }
    if (useMediaQuery({minWidth: 768, maxWidth: 1270})) {
        imageCount = 4;
    }
    console.log(imageCount);


    let galleryImages = [];
    for (let i = 0; i < gallery.length; i++) {
        if (i < imageCount) {

            galleryImages.push(
                <div className="d-flex gallery-content justify-content-center align-items-center "
                     key={i}>
                    <button type="button" data-bs-toggle="modal"
                            data-bs-target="#exampleModal">
                        <img src={gallery[i].image} alt={gallery[i].alt_text} className="d-flex" onClick={handleClick}
                             id={i}/>
                    </button>
                </div>
            )
        }

    }
    return (
        <>
            <div className="d-flex flex-grow-1 image-gallery rtl justify-content-center"
            >
                {galleryImages}
            </div>
            <GalleryModel gallery={gallery} slideNo={slideNum}/>
        </>
    )
}

export default LgPicture;