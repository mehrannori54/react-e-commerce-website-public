import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination} from "swiper";
import {NavLink} from "react-router-dom";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";


const SmPicture = ({main_images, gallery, alt}) => {
    document.body.classList.remove('modal-open');
    document.body.lastChild.classList.remove("modal-backdrop")
    // document.removeChild(element)
    // (document.getElementsByClassName("modal-backdrop")).classList.remove("modal-backdrop")

    if (!main_images || main_images.length === 0) return <p>Can not find any slide, sorry</p>;

    return (
        <div className="d-flex flex-shrink-0 product_gallery rtl">
            <Swiper
                style={{
                    "--swiper-navigation-color": "#fbfbfb",
                    "--swiper-navigation-size": "25px",
                    "--swiper-pagination-color": "#fbfbfb",
                }}
                slidesPerView={1}
                navigation={true}
                reverseDirection={true}
                autoplay={{
                    delay: 10000,
                    disableOnInteraction: false,
                }}
                pagination={{
                    clickable: true,
                }}
                lazy={true}
                modules={[Navigation, Pagination]}
                className="mySwiper"

            >
                {gallery.map((image) => {
                    return (
                        <SwiperSlide>
                            <NavLink to={image.url}>
                                <img src={image.image} alt={image.title} loading="lazy"/>
                                <div className="swiper-lazy-preloader swiper-lazy-preloader-white"/>
                            </NavLink>
                        </SwiperSlide>

                    )
                })}
            </Swiper>

        </div>
    )
}
export default SmPicture;