const MiddleDetails = ({details}) => {
    return (

        details.map((detail, i) => {
            if (i <= 3) {
                return (
                    <div className="d-flex flex-row ">

                        <div className="middle-detail">
                            {detail.specification.name}:
                        </div>
                        <div className="middle-detail-value">
                            {detail.value}
                        </div>
                    </div>
                )

            }
        })
    )
}
export default MiddleDetails;