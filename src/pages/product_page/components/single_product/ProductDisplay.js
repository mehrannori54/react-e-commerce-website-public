import MainSector from "./main_sector/MainSector";
import './main_sector/css/main_sector.sass'

function ProductDisplay(props) {

    const {product_data} = props;
    // console.log(product_data);
    // console.log(product_data.title);

    //if (!product_data || product_data.length === 0) return <p>Can not find any slide, sorry</p>;
    return (
        <MainSector product={product_data}/>
    )

}

export default ProductDisplay